package com.Roj.graphics;

import com.Roj.shapes.Line;
import com.Roj.shapes.Vec2;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class GraphicsCanvas extends JPanel {

    private static final Vec2 START_POS = new Vec2(100, 580);

    private static final int HOUSE_SIDE_WIDTH = 400;

    private static final Color LINE_COLOR = Color.decode("#000300");
    private static final Color FILL_COLOR = Color.decode("#FAFF00");
    private static final Color RECOLOR_LINE = Color.decode("#FF01FB");

    private int recolorIndex = 0;

    private final ArrayList<Polygon> polygonsToDraw = new ArrayList<>();

    {
        Polygon housePolygon = new Polygon();
        // BOTTOM LEFT
        housePolygon.addPoint(START_POS.x, START_POS.y);
        // TOP LEFT
        housePolygon.addPoint(START_POS.x, START_POS.y - HOUSE_SIDE_WIDTH);
        // TOP RIGHT
        housePolygon.addPoint(START_POS.x + HOUSE_SIDE_WIDTH, START_POS.y - HOUSE_SIDE_WIDTH);
        // BOTTOM RIGHT
        housePolygon.addPoint(START_POS.x + HOUSE_SIDE_WIDTH, START_POS.y);

        polygonsToDraw.add(housePolygon);

        Polygon roofPolygon = new Polygon();
        roofPolygon.addPoint(START_POS.x, START_POS.y - HOUSE_SIDE_WIDTH);
        roofPolygon.addPoint(START_POS.x + HOUSE_SIDE_WIDTH, START_POS.y - HOUSE_SIDE_WIDTH);
        roofPolygon.addPoint(START_POS.x + HOUSE_SIDE_WIDTH / 2, START_POS.y - HOUSE_SIDE_WIDTH - 150);

        polygonsToDraw.add(roofPolygon);
    }

    private final ArrayList<Polygon> drawnPolygons = new ArrayList<>();

    private final ArrayList<Vec2> linesToDrawSuccessive = new ArrayList<>(Arrays.asList(
                new Vec2(0, -HOUSE_SIDE_WIDTH),
                new Vec2(HOUSE_SIDE_WIDTH, 0),
                new Vec2(-HOUSE_SIDE_WIDTH, HOUSE_SIDE_WIDTH),
                new Vec2(HOUSE_SIDE_WIDTH, 0),
                new Vec2(0, -HOUSE_SIDE_WIDTH),
                new Vec2(-HOUSE_SIDE_WIDTH / 2, -150),
                new Vec2(-HOUSE_SIDE_WIDTH / 2, 150),
                new Vec2(HOUSE_SIDE_WIDTH, HOUSE_SIDE_WIDTH)
            ));

    private final ArrayList<Line> drawnLines = new ArrayList<>();


    private static final int LINE_DRAW_DELAY = 500;
    private static final int MICROSLEEP_DURATION = 10;

    private static final int LINE_WIDTH = 5;

    public GraphicsCanvas() {
    }

    public void startHouseDrawing() {
        Thread houseDrawingThread = new Thread(this::houseDrawingThreadStart);
        houseDrawingThread.start();
    }

    private void threadDelaySleep(long duration)
    {
        try
        {
            Thread.sleep(duration);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void threadDelaySleepLong()
    {
        threadDelaySleep(LINE_DRAW_DELAY);
    }

    private void houseDrawingDrawPolygon(Polygon polygon)
    {
        synchronized (drawnPolygons)
        {
            drawnPolygons.add(polygon);
        }

        repaint();

        threadDelaySleepLong();
    }

    private void houseDrawingDrawLine(Line line)
    {
        for(int step = 0; step <= LINE_DRAW_DELAY; step += MICROSLEEP_DURATION)
        {
            float perc = step / (float)LINE_DRAW_DELAY;
            threadDelaySleep(MICROSLEEP_DURATION);

            Vec2 adjustedEndPoint = line.getInterpolatedPoint(perc);

            synchronized (drawnLines)
            {
                if(step == 0)
                {
                    drawnLines.add(new Line(line.getStart(), adjustedEndPoint));
                }

                Line lastLine = drawnLines.get(drawnLines.size() - 1);
                lastLine.setEnd(adjustedEndPoint);
            }

            repaint();
        }

        threadDelaySleepLong();
    }

    private void drawAllLines()
    {
        for(int i = 0; i < linesToDrawSuccessive.size(); ++i)
        {
            Line l = new Line();
            l.setStart(START_POS);

            for(int j = 0; j <= i; ++j)
            {
                Vec2 change = linesToDrawSuccessive.get(j);

                if(j != 0)
                {
                    Vec2 pChange = linesToDrawSuccessive.get(j - 1);
                    l.setStart(new Vec2(l.x1 + pChange.x, l.y1 + pChange.y));
                }
                l.setEnd(new Vec2(l.x1 + change.x, l.y1 + change.y));

            }
            houseDrawingDrawLine(l);
        }
    }

    private void recolorNextLine()
    {
        ++recolorIndex;
        repaint();
        threadDelaySleepLong();
    }

    private void recolorAllLines()
    {
        for(int i = 0; i < drawnLines.size(); ++i)
        {
            recolorNextLine();
        }
    }

    private void drawAllPolygons()
    {
        for(Polygon p : polygonsToDraw)
        {
            houseDrawingDrawPolygon(p);
        }
    }

    private void houseDrawingThreadStart()
    {
        threadDelaySleepLong();
        drawAllLines();
        drawAllPolygons();
        recolorAllLines();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;

        g2D.setColor(FILL_COLOR);

        synchronized (drawnPolygons)
        {
            for(Polygon polygon : drawnPolygons)
            {
                g2D.fillPolygon(polygon);
            }
        }


        g2D.setStroke(new BasicStroke(LINE_WIDTH));

        synchronized (drawnLines)
        {
            for(int i = 0; i < drawnLines.size(); ++i)
            {
                Line line = drawnLines.get(i);
                g2D.setColor(i < recolorIndex  ? RECOLOR_LINE : LINE_COLOR);
                g2D.drawLine(line.x1, line.y1, line.x2, line.y2);
            }
        }


    }
}
