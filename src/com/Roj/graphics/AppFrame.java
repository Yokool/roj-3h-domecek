package com.Roj.graphics;

import javax.swing.*;
import java.awt.*;

public class AppFrame extends JFrame
{
    private static final String APPLICATION_TITLE = "Roj - Grafika - Domeček";

    private final GraphicsCanvas graphicsCanvas = new GraphicsCanvas();

    public AppFrame()
    {
        initLayout();
        initChildren();
    }

    private void initLayout()
    {
        GridBagLayout layout = new GridBagLayout();

        layout.columnWidths = new int[] {
                0,
                600,
                0
        };
        layout.rowHeights = new int[] {
                600
        };

        setLayout(layout);
    }

    private void initChildren()
    {
        GridBagConstraints graphicsConstraints = new GridBagConstraints();
        graphicsConstraints.gridx = 1;
        graphicsConstraints.gridy = 0;
        graphicsConstraints.fill = GridBagConstraints.BOTH;

        add(graphicsCanvas, graphicsConstraints);
    }

    public void startApplication()
    {
        // UGF2ZWwgUm9qIDNoCg==
        setTitle(APPLICATION_TITLE);
        setSize(1080, 720);
        setLocationRelativeTo(null);
        setResizable(true);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        onFrameShown();
    }

    public void onFrameShown()
    {
        graphicsCanvas.startHouseDrawing();
    }

}
