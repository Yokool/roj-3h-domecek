package com.Roj.shapes;

public class Line
{

    public int x1;
    public int y1;
    public int x2;
    public int y2;

    public Vec2 getInterpolatedPoint(float weight)
    {
        int xInterp = (int) (x1 + (x2 - x1) * weight);
        int yInterp = (int) (y1 + (y2 - y1) * weight);
        return new Vec2(xInterp, yInterp);
    }

    public Line()
    {

    }

    public Vec2 getStart()
    {
        return new Vec2(x1, y1);
    }

    public Vec2 getEnd()
    {
        return new Vec2(x2, y2);
    }

    public Line(Vec2 p1, Vec2 p2)
    {
        this(p1.x, p1.y, p2.x, p2.y);
    }


    @Override
    public String toString() {
        return "Line{" +
                "x1=" + x1 +
                ", y1=" + y1 +
                ", x2=" + x2 +
                ", y2=" + y2 +
                '}';
    }

    public void setStart(Vec2 vec2)
    {
        this.x1 = vec2.x;
        this.y1 = vec2.y;
    }

    public void setEnd(Vec2 vec2)
    {
        this.x2 = vec2.x;
        this.y2 = vec2.y;
    }

    public Line(int x1, int y1, int x2, int y2)
    {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

}
