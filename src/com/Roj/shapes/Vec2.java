package com.Roj.shapes;

public class Vec2
{

    public int x;
    public int y;

    @Override
    public String toString() {
        return "Vec2{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public Vec2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

}
